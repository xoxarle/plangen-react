import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import superagent from 'superagent';
import { Planet } from './components/planet';

const JSON_TEST_DATA = {"starName":"Goldfield","starType":"G","starLum":2,"planets":[{"planetName":"Azalia","planetType":"Hostile","diameter":14000,"num_moons":0,"zone":"A","features":[]},{"planetName":"Silverstreet","planetType":"Desert","diameter":10000,"num_moons":0,"zone":"A","features":[]},{"planetName":"Joiner","planetType":"Marginal","diameter":10000,"num_moons":0,"zone":"B","features":[],"day":16,"density":5.88,"gravity":0.84,"mineral":37.8,"orbit":334,"temperature":35},{"planetName":"Salinas","planetType":"Desert","diameter":7000,"num_moons":1,"zone":"B","features":[]},{"planetName":"Uwchland","planetType":"Giant","diameter":120000,"num_moons":12,"zone":"C","features":[]},{"planetName":"Gheen","planetType":"Giant","diameter":150000,"num_moons":13,"zone":"C","features":[]},{"planetName":"Vallejo","planetType":"Giant","diameter":130000,"num_moons":9,"zone":"C","features":[]},{"planetName":"Aniwa","planetType":"Giant","diameter":130000,"num_moons":12,"zone":"C","features":[]}]}



function StarClass(props) {
    var CLASSES=["F","G","K","M"];
    return (
        <select value={props.starclass} onChange={props.onChange}>
            { 
                CLASSES.map( c => {
                    return <option value={c}>{c}</option>
                })
            }
        </select>
    );
}

function StarLum(props) {
    var LUMS=[...Array(10).keys()];
    return (
        <select value={props.starlum}  onChange={props.onChange}>
            { 
                LUMS.map( c => {
                    return <option value={c}>{c}</option>
                })
            }
        </select>
    );
}

class Solsys extends React.Component {
    constructor (props) {
        super(props) ;
        this.state = {
            solsys    : JSON_TEST_DATA,
            starclass : "G",
            starlum   : 2,
        }
    }

    async getNewData() {
        let url ="http://localhost:3001/solsys";
        console.log(url);
        superagent.get(url)
        .then(msg => {
            this.setState( {solsys  : msg.body });
        });
    }

    async getStarData() {
        let url ="http://localhost:3001/solsys";
        console.log(url);
        superagent
        .post(url)
        .send({starclass : this.state.starclass, starlum : this.state.starlum})
        .set('accept','json')
        .then(msg => {
            this.setState( {solsys  : msg.body });
        });
    }    

    handleStarclass = (event) => {
        this.setState({ starclass : event.target.value});
    }

    handleStarLum = (event) => {
        this.setState({ starlum : event.target.value});
    }

    


    render() {       
        return (
            <div className="solsys">
                <h2>{this.state.solsys.starName} - {this.state.solsys.starType}{this.state.solsys.starLum}</h2>
                <div className="buttonbar">
                    <div class="btn-left">
                        <button onClick={async () => { await this.getNewData() }}>Random solar system</button>
                    </div>
                    <div class="btn-right">
                        <StarClass starclass={this.state.starclass} onChange={this.handleStarclass}/>                    
                        <StarLum starlum={this.state.starlum} onChange={this.handleStarLum}/>
                        <button onClick={async () => { await this.getStarData() }}>Get solar system with this data</button>                    
                    </div>
                </div>
                {
                    this.state.solsys.planets.map(p => {
                        const kw="planet "+p.planetType;
                        return <div className={kw} key={p.planetName}><Planet planet={p} /></div> 
                    })
                }

            </div>
        );
    }
}

ReactDOM.render(
    <Solsys />,
    document.getElementById('root')
  );
  
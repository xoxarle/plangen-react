import React from 'react';


export function PlanetFeatures(props) {
    return (
    <tr>
        <th>Features</th>
        <td>
            <h4>Physical Features:</h4>
            <p>
            Density: {props.planet.density},
            Gravity: {props.planet.gravity},
            Year in earth days: {props.planet.year},
            Day in hours: {props.planet.day},
            Temperature in Celsius: {props.planet.temperature},
            Water: {props.planet.water},
            Ice: {props.planet.ice},
            Landmass: {props.planet.landmass}
            </p>
            <h4>Biological Features</h4>
            <p>
            Life development: {props.planet.life_development}, life chemistry: {props.planet.life_chemistry}
            </p>
        </td>
    </tr>        
    );
}

export class Planet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            planet : props.planet
        };
    }

    get showFeatures() {
        if (this.state.planet != null) {
            let p = this.state.planet;
            return (p.planetType === "Marginal" || p.planetType === "Earthlike");
        }
        else
            return false;
    }    

    render() {
        if (this.state.planet == null) {
            return (
                <div>
                    <h3>Undefined planet</h3>
                </div>
            );
        } else {
            return (
                <div>
                    <h3>{this.state.planet.planetName}</h3>
                    <table>
                        <tbody>
                        <tr>
                            <th>Type</th><td>{this.state.planet.planetType}</td>
                        </tr>
                        <tr>
                            <th>Diameter</th><td>{this.state.planet.diameter}</td>
                        </tr>
                        <tr>
                            <th>Moons</th><td>{this.state.planet.num_moons}</td>
                        </tr> 
                        {
                            this.showFeatures ?
                            <PlanetFeatures planet={this.state.planet} />
                            : 
                            null
                        } 
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}